# Texlive 2019 Container

Usage: `docker run --rm -v $(pwd):/workspace linvirt/texlive-2019 latexmk -output-directory=<directory> -lualatex <tex file>`

where $(pwd) contains the directory with the tex files
